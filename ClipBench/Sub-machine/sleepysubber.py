import os
import time
import keyboard
import re

twitch_delay=60
print_key = "menu"
last_time = 0
not_assigned = True

CLIP_DURATION=30
LINE_LIMIT=220
DEFAULT_DELAY=1

'''
REV1
class time_item(object):
    def __init__(self, timecode):
        self.m = int(timecode[3:5])
        self.s = int(timecode[6:8])
        self.ds = int(timecode[9:11])


class sub_item(object):
    """docstring for sub_item"""
    def __init__(self, time_line, text):
        self.start_time = time_item(time_line[:12])
        self.end_time = time_item(time_line[17:])
        self.text = text
'''


class time_item(object):
    def __init__(self, timecode):
        self.s = timecode // 60
        self.ps = timecode % 60
        self.m = self.s // 60
        self.s = self.s % 60
    def davinci_print(self):
        return "00:" + str(self.m) + ":" + str(self.s) + ":" + str(self.ps)


class sub_item(object):
    """start_time/duration in ps = 1/60s"""
    def __init__(self, time_line, text, split_num = 0, split_chunks = 1, split_amount = 0):
        p_start_time = int(time_line[3:5])*3600 + int(time_line[6:8])*60 + int(time_line[9:11])*60//100
        p_duration = (int(time_line[20:22])*3600 + int(time_line[23:25])*60 + int(time_line[26:28])*60//100 - p_start_time)//(split_amount+1)
        self.start_time = p_start_time + p_duration*split_num
        self.duration = p_duration*split_chunks
        self.text = text
    def print(self):
        print(f'{self.text}: starts at {self.start_time}, lasts for {self.duration}')

def line_printer(text):
    print(f'Line is too long: {text}')
    for i in range(1,LINE_LIMIT):
        print(" ", end="")
    print('Largest split here^')

def split_string(input_string, split_word):
    result = []
    split_word = split_word
    split_point = input_string[:LINE_LIMIT].rfind(split_word) + len(split_word) + 1

    sub_part1 = input_string[:split_point].strip()
    result.append(sub_part1)

    sub_part2 = input_string[split_point:].strip()
    if (len(sub_part2) > LINE_LIMIT):
        line_printer(sub_part2)
        seek_word = input('need another split for that - enter the splitting word (would be last one of first line): ')
        sub_parts = split_string(sub_part2, seek_word)
        for sub_part in sub_parts:
            result.append(sub_part)
    else:
        result.append(sub_part2)
    return result

def list_sum(num_list):
    result = 0
    for i in num_list:
        result = result + int(i)
    return result

   
    pattern = r'\b' + re.escape(split_word) + r'\b(?=\W|$)'
    substrings = re.split(pattern, input_string, flags=re.IGNORECASE)
    return [substring.strip() for substring in substrings if substring.strip()]


#MAIN BODY
#init
sub_items_array = []

srt_file = input("? input srt file name: ")
f = open(srt_file, "r")


#PROCESSING FILE INTO CLASS ARRAY
while line := f.readline():
    time_line = f.readline()
    sub_line = f.readline().strip()
    #thick line
    if (len(sub_line) > LINE_LIMIT):
        #splitting
        line_printer(sub_line)
        seek_word = input('enter the splitting word (would be last one of first line): ')
        sub_lines = split_string(sub_line,seek_word)
        print('You\'ve got:\n============================================')
        for line in sub_lines:
            print(line)
        print("============================================")

        #getting sub_clump order
        sub_clump = [1]
        sub_index = 0
        for i in range(1, len(sub_lines)):
            print("--------------------------------------------")
            print(f'connecting "{sub_lines[i-1]}" and "{sub_lines[i]}"')
            choice = input('do you want [1 double line] or [2 single lines] subtitle items there (1/2): ')
            if choice == '1':
                sub_clump[sub_index] += 1
            elif choice == '2':
                sub_index += 1
                sub_clump.append(1)
            print("--------------------------------------------")

        #combining
        split_amount = list_sum(sub_clump) #sum of all chunks
        split_num = 0

        for i in sub_clump:
            combined_line = sub_lines[split_num] #getting the first line in clump item
            split_num += 1
            split_chunks = int(i)
            while split_chunks != 1:
                split_chunks -= 1
                combined_line += '\n' + sub_lines[split_num]
                split_num += 1
            split_chunks = int(i)
            sub_items_array.append(sub_item(time_line,combined_line, split_num - split_chunks, split_chunks, split_amount))
    else:   
        sub_items_array.append(sub_item(time_line, sub_line))
    f.readline()
f.close()

print('set doodad at the start of the clip you sub, select text+ you want to use as template, hit ctrl ')
keyboard.wait('right ctrl')
time.sleep(DEFAULT_DELAY)

#get your template
keyboard.send('ctrl+x')

#adding clips at right times
cur_time = 0

for sub_items in sub_items_array:
    #skipping
    duration_item = time_item(sub_items.duration)
    print(f'moving {sub_items.start_time - cur_time}')
    for i in range(0,sub_items.start_time - cur_time):
        keyboard.send('right')
    #adding clip, fixing duration
    time.sleep(DEFAULT_DELAY*2)
    keyboard.send('ctrl+v')
    time.sleep(DEFAULT_DELAY)
    keyboard.send('ctrl+d')
    time.sleep(DEFAULT_DELAY)
    keyboard.write(duration_item.davinci_print())
    time.sleep(DEFAULT_DELAY)
    keyboard.send('enter')
    time.sleep(DEFAULT_DELAY)

    cur_time = sub_items.start_time + CLIP_DURATION

#typing
print(f'select 1st subtitle clip, select textbox in inspector')
for sub_items in sub_items_array:
    keyboard.wait('right ctrl')
    time.sleep(DEFAULT_DELAY)
    keyboard.send('ctrl+a')
    keyboard.write(sub_items.text)
    print(f'select next subtitle clip, select textbox in inspector')

quit()
