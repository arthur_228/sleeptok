Proper readme would be too much to write, will likely do a walkthrough. Otherwise:

# Installation/Setup
* All is fairly simple here: click download icon near top right, select whatever archive format you like
* Davinci doesn't read relative paths as far as I know, so unarchive all the files to `D:\ClipBench`, so that Davinci project file (`.drp`) is in that folder.
* Fill the missing files (didn't include them, since I don't have the rights), including:
    * logo `D:\ClipBench\Static\outro_images\sleepypurin_logo_4281.png`
    * emotes (with their original names) `D:\ClipBench\Static\emotes\*`

Now you can work with created template, by double clicking Davinci project file (`.drp`)! Workflow and such will review later on.

# sleepysubber Installation/Setup
Has exact installation process as *sleepyclipper*. If you already went through it, then you should be fine to launch it from the get-go: double click `sleepysubber.py` and follow the prompts. Will dork-proof this one in further revisions/on demand.